class Comment < ActiveRecord::Base
 #Relations
 belongs_to :post
 #Validations
 validates_presence_of :post_id
 validates_presence_of :body
end
