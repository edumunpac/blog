class Post < ActiveRecord::Base
 #Relations
 has_many :comments, dependent: :destroy
 #Validations
 validates_presence_of :title
 validates_presence_of :body

end
